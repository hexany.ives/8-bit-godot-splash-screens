# 8-Bit Godot Splash Screens

4 variants of 8-bit pixel art splash screens for your Godot Engine Projects.

<p align="center"><img src="preview.png"/></p>

## License

- [ ] The Godot logo is made by Andrea Calabró and is licensed under [CC-BY-4.0](https://github.com/godotengine/godot/blob/master/LOGO_LICENSE.md).
- [ ] For additional info regarding usage of Godot logo, go [here](https://godotengine.org/press/).

## More of my stuff

I post my art (and sometimes other stuff) on my [Mastodon page](https://artsio.com/@HexanyIves).
